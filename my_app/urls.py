from django.urls import path
from .my_http.views.config_view import *

urlpatterns = [

    # quan ly config
    path('config-group', ManagerConfig.as_view({'post': 'configGroup'})),
    path('config-show', ManagerConfig.as_view({'post': 'configShow'})),
    path('config-add', ManagerConfig.as_view({'post': 'configAdd'})),
    path('config-update', ManagerConfig.as_view({'post': 'configUpdate'})),
    path('config-delete', ManagerConfig.as_view({'post': 'configDelete'})),
    
]
