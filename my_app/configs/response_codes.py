# code = 3 : quay ve man hinh Login

response_codes_data = {
    "RESPONSE_REFRESH_TOKEN_FAILED": {
        "code": 3,
        "vi": "Phiên làm việc đã hết",
        "en": "Session is expired"
    },
    "ACCESS_TOKEN_FAILED": {
        "code": 3,
        "vi": "Access Token không chính xác",
        "en": "Access Token invalid"
    },
    "RESPONSE_GEN_TOKEN_FAILED": {
        "code": 3,
        "vi": "Generate token thất bại",
        "en": "Generate token failed"
    },
    "RESPONSE_USER_NOT_FOUND_BY_USER_ID": {
        "code": 3,
        "vi": "Không tim thấy User ID này",
        "en": "User not found by user ID"
    },
    "CONNECT_REDIS_FAILED": {
        "code": 8,
        "vi": "Kết nối thất bại",
        "en": "Connect failed"
    }
}

STATUS_CODE_ERROR_LOGIC = 4
STATUS_CODE_NO_DATA = 6
STATUS_CODE_SUCCESS = 1
STATUS_CODE_INVALID_INPUT = 5
STATUS_CODE_FAILED = 0
STATUS_CODE_ERROR_SYSTEM = 4

MESSAGE_API_SUCCESS = "Thành công"
MESSAGE_API_FAILED = "Thất bại"
MESSAGE_API_NO_DATA = "Không có thông tin"
MESSAGE_API_NO_INPUT = "Vui lòng nhập đầy đủ thông tin"

MESSAGE_API_ERROR_SYSTEM = "Lỗi hệ thống"
MESSAGE_API_ERROR_LOGIC = "Thất bại"

