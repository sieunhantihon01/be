SERVICE_CONFIG = {
    'HO-PROFILE': {
        'local': 'http://127.0.0.1:8001/mypt-ho-profile-api/',
        'staging': 'http://mypt-ho-profile-api-staging/mypt-ho-profile-api/',
        'production': 'http://mypt-ho-profile-api/mypt-ho-profile-api/',
        'update_employees_info_with_permissions_and_fea_roles': {
            'func': 'update-employees-info-with-permissions-and-fea-roles',
            'method': 'POST'
        },
        'check_email_has_fea_roles_by_codes': {
            'func': 'check-email-has-fea-roles-by-codes',
            'method': 'POST'
        }
    },
    'HO-LOGS': {
        'local': 'http://127.0.0.1:8000/mypt-ho-logs-api/',
        'staging': 'http://mypt-ho-logs-api-staging/mypt-ho-logs-api/',
        'production': 'http://mypt-ho-logs-api/mypt-ho-logs-api/',
        'post_log_user_auth': {
            'func': 'post-log-user-auth',
            'method': 'POST',
            'web_route': '/logout',
            'api_route': '/mypt-ho-auth-api/logout',
        }
    }
}
