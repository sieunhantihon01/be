# Models
from ..models.mypt_config_group import *
from ..models.mypt_config import *
# Serializers
from ..serializers.mypt_config_group_serializer import *
from ..serializers.mypt_config_serializer import *
# Helpers
from rest_framework.viewsets import ViewSet
from ...my_core.helpers.response import *
from ..paginations.custom_pagination import StandardPagination
from ..validations.config_validation import *


class ManagerConfig(ViewSet):

    # danh sach cac group config
    def configGroup(self, request):
        queryset = MyPTConfigGroup.objects.filter(group_status='enabled').all()
        if not queryset.exists():
            return response_data({}, statusCode=0, message="No data 'enabled'")
        serializer = MyPTConfigGroupSerializer(queryset, many=True)
        return response_data(serializer.data)

    # show danh sach config dua tren group
    def configShow(self, request):
        try:
            group_ID = request.data.get('group_ID')
            validate_group_id(group_ID)

            # khong co group nao enabled
            group = MyPTConfigGroup.objects.get(group_ID=group_ID)
            if group.group_status != 'enabled':
                return response_data({}, statusCode=0, message="Group is not enabled")

            queryset = MyPTConfig.objects.filter(group_ID=group_ID, config_status='enabled')
            if not queryset.exists():
                return response_data({}, statusCode=0, message="khong co du lieu voi 'enabled' ")

            page_size = request.query_params.get('page_size', 10)
            paginator = StandardPagination()
            paginator.page_size = page_size
            paginated_queryset = paginator.paginate_queryset(queryset, request)

            serializer = MyPTConfigSerializer(paginated_queryset, many=True)
            total = queryset.count()
            data = serializer.data
            count = len(data)

            return response_paginator(total, int(page_size), data, count)
        except MyPTConfigGroup.DoesNotExist:
            return response_data({}, statusCode=0, message="group_id does not exist")
        except Exception as e:
            return response_data({}, statusCode=0, message=str(e))

    # them config moi
    def configAdd(self, request):
        try:
            data = request.data
            group_ID = data.get('group_ID')
            validate_group_id(group_ID)

            group = MyPTConfigGroup.objects.get(group_ID=group_ID)
            if group.group_status != 'enabled':
                return response_data({}, statusCode=0, message="Group khong 'enabled'")

            # check
            config_type = data.get('config_type')
            if not config_type:
                return response_data({}, statusCode=0, message="can co config_type")
            if config_type not in ['constant', 'message', 'remote']:
                return response_data({}, statusCode=0, message="config_type khong hop le")

            config_key = data.get('config_key')
            if not config_key:
                return response_data({}, statusCode=0, message="can co config_key")
            if MyPTConfig.objects.filter(config_key=config_key).exists():
                return response_data({}, statusCode=0, message="config_key da ton tai")

            config_status = data.get('config_status', 'enabled')
            if config_status not in ['enabled', 'disabled', 'deleted']:
                return response_data({}, statusCode=0, message='config_status khong hop le')
            
            owner = data.get('owner')
            if not owner:
                return response_data({}, statusCode=0, message='can co owner')

            new_config = MyPTConfig(
                group_ID = group_ID,
                config_ID = None,
                config_type = config_type,
                config_key = config_key,
                config_status = config_status,
                owner = owner,
                config_value = data.get('config_value'),
                config_description_vi = data.get('config_description_vi'),
                config_description_en = data.get('config_description_en'),
                note = data.get('note')
            )
            new_config.save()

            return response_data({}, statusCode=1, message="Config them thanh cong")
        except MyPTConfigGroup.DoesNotExist:
            return response_data({}, statusCode=0, message="group_id does not exist")
        except Exception as e:
            return response_data({}, statusCode=0, message=str(e))

    # thay doi config
    def configUpdate(self, request):
        try:
            data = request.data
            group_ID = data.get('group_ID')
            validate_group_id(group_ID)

            # check config_ID
            config_ID = data.get('config_ID')
            validate_config_ID(config_ID)

            try:
                config = MyPTConfig.objects.get(group_ID=group_ID, config_ID=config_ID)
            except MyPTConfig.DoesNotExist:
                return response_data({}, statusCode=0, message=f"khong ton tai config voi {group_ID} va {config_ID}")

            # check config_type
            config_type = data.get('config_type', config.config_type)
            if config_type not in ['constant', 'message', 'remote']:
                return response_data({}, statusCode=0, message="config_type khong hop le")

            # config_key
            config_key = data.get('config_key', config.config_key)
            if config_key != config.config_key:
                if MyPTConfig.objects.filter(config_key=config_key).exists():
                    return response_data({}, statusCode=0, message="config_key da ton tai")

                    # config_status
            config_status = data.get('config_status', config.config_status)
            if config_status not in ['enabled', 'disabled', 'deleted']:
                return response_data({}, statusCode=0, message="config_status khong hop le")

            # update
            config.config_type = config_type
            config.config_key = config_key
            config.config_status = config_status
            config.config_value = data.get('config_value', config.config_value)
            config.owner = data.get('owner', config.owner)
            config.config_description_vi = data.get('config_description_vi', config.config_description_vi)
            config.config_description_en = data.get('config_description_en', config.config_description_en)
            config.note = data.get('note', config.note)
            config.save()

            return response_data({}, statusCode=1, message="Config cap nhat thanh cong")
        except MyPTConfigGroup.DoesNotExist:
            return response_data({}, statusCode=0, message="group_ID does not exist")
        except Exception as e:
            return response_data({}, statusCode=0, message=str(e))

    # xoa config
    def configDelete(self, request):
        try:
            data = request.data
            group_ID = data.get('group_ID')
            validate_group_id(group_ID)  # Validate group_ID

            # check config_ID
            config_ID = data.get('config_ID')
            validate_config_ID(config_ID)

            try:
                config = MyPTConfig.objects.get(group_ID=group_ID, config_ID=config_ID)
            except MyPTConfig.DoesNotExist:
                return response_data({}, statusCode=0,
                                     message=f"Config group_ID: {group_ID} va config_ID: {config_ID} khong ton tai")

            config.delete()
            return response_data({}, statusCode=1, message="Config xoa ok")

        except MyPTConfigGroup.DoesNotExist:
            return response_data({}, statusCode=0, message="group_ID does not exist.")
        except Exception as e:
            return response_data({}, statusCode=0, message=str(e))