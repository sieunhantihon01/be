from my_app.my_http.serializers.log_auto_permission_serializer import LogPermissionSerializer
from datetime import datetime

class LogAutoPermissionHandler():
    def save_log_permission(self, data_log, key_log, created_by):
        try:

            data_insert = {
                "dataLog": data_log,
                "keyLog": key_log,
                "createdBy": created_by
            }
            insert_data = LogPermissionSerializer(data=data_insert)
            if insert_data.is_valid():
                insert_data.save()
        except Exception as ex:
            print("save_log_permission >> Error/loi: {}".format(ex))