from my_app.my_http.models.user_infos import UserInfos
from my_app.my_http.serializers.user_infos_serializer import UserInfosSerializer

class UserInfosHandler:
    def getAllUsers(self):
        userInfoQs = UserInfos.objects.all()
        userInfo_ser = UserInfosSerializer(userInfoQs, many=True)
        userInfosArr = userInfo_ser.data
        return userInfosArr

    def getUserByEmail(self, email):
        email = email.lower()
        usQs = UserInfos.objects.filter(email=email)[0:1]
        userInfo_ser = UserInfosSerializer(usQs, many=True)
        usersArr = userInfo_ser.data
        # print(usersArr)
        if len(usersArr) > 0:
            userInfoItem = usersArr[0]
            return userInfoItem
        else:
            return None

    def getUserByUserId(self, userId):
        usQs = UserInfos.objects.filter(user_id=userId)[0:1]
        userInfo_ser = UserInfosSerializer(usQs, many=True)
        usersArr = userInfo_ser.data
        # print(usersArr)
        if len(usersArr) > 0:
            userInfoItem = usersArr[0]
            return userInfoItem
        else:
            return None

    def createUser(self, email, userInfo):
        newUser = UserInfos()
        newUser.email = email.lower()
        newUser.full_name = userInfo.get("fullName")
        newUser.app_language = userInfo.get("lang", "vi")

        if userInfo.get("hoDateLogin", None) is not None:
            newUser.ho_date_login = userInfo.get("hoDateLogin")

        newUser.unread_notify = 1
        newUser.is_deleted = 0
        resInsert = newUser.save()
        newUserId = newUser.user_id
        print("new user id : " + str(newUserId))

        if int(newUserId) > 0:
            return {"resCreate": "SUCCESS", "userId": int(newUserId)}
        else:
            return {"resCreate": "FAILED"}

    def updateUserInfoByUserId(self, userId, userInfo = {}):
        # cach 1 :
        updatedFields = []
        usQs = UserInfos.objects.get(user_id=userId)
        if userInfo.get("fullName", None) is not None:
            usQs.full_name = userInfo.get("fullName")
            updatedFields.append("full_name")

        if userInfo.get("hoDateLogin", None) is not None:
            usQs.ho_date_login = userInfo.get("hoDateLogin")
            updatedFields.append("ho_date_login")

        if userInfo.get("hoDateLatestRefreshToken", None) is not None:
            usQs.ho_date_latest_refresh_token = userInfo.get("hoDateLatestRefreshToken")
            updatedFields.append("ho_date_latest_refresh_token")

        if userInfo.get("lang", None) is not None:
            usQs.app_language = userInfo.get("lang")
            updatedFields.append("app_language")

        resUpdate = usQs.save(update_fields=updatedFields)

        return resUpdate

    def get_user_id_from_email(self, email):
        user_id = None
        try:
            queryset = UserInfos.objects.filter(email=email).values_list('user_id', flat=True)
            if len(queryset) > 0:
                user_id = queryset[0]
            else:
                user_info = {
                    "fullName": '',
                    "lang": "vi"
                }
                dict_insert = self.createUser(email, user_info)
                user_id = dict_insert.get('userId', '')

        except Exception as ex:
            print("get_user_id_from_email >> Error/loi: {}".format(ex))
        return user_id


    def getUsersByEmails(self, emails):
        usQs = UserInfos.objects.filter(email__in=emails)
        users_ser = UserInfosSerializer(usQs, many=True)
        usersArr = users_ser.data
        # print(usersArr)
        usersList = []
        if len(usersArr) > 0:
            for userItem in usersArr:
                usersList.append({
                    "userId": int(userItem["user_id"]),
                    "email": userItem["email"],
                    "fullName": userItem["full_name"],
                    "deviceId": userItem["device_id"],
                    "deviceName": userItem["device_name"],
                    "devicePlatform": userItem["device_platform"]
                })
            return usersList
        else:
            return []