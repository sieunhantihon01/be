from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from ..models.mypt_config_group import MyPTConfigGroup

class MyPTConfigGroupSerializer(ModelSerializer):
    class Meta:
        model = MyPTConfigGroup
        fields = ['group_ID', 'group_title', 'group_key']
        # fields = '__all__'
