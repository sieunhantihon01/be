from rest_framework.serializers import *
from ..models.mypt_config_group import *
from ..models.mypt_config import *

def validate_group_id(group_ID):
    if group_ID is None:
        raise ValidationError({"group_ID": "can co group_ID"})
    if not isinstance(group_ID, int):
        raise ValidationError({"group_ID": "group_ID phai la integer"})
    if group_ID < 0:
        raise ValidationError({"group_ID": "group_ID phai la nguyen duong"})
    if not MyPTConfigGroup.objects.filter(group_ID=group_ID).exists():
        raise ValidationError({"group_ID": "group_ID does not exist"})

def validate_config_ID(config_ID):
    if not config_ID:
        raise ValidationError("can co config_ID")
    if not isinstance(config_ID, int):
        raise ValidationError("config_ID phai la integer")
    if config_ID < 0:
        raise ValidationError("config_ID phai la nguyen duong")
