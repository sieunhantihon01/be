import requests
import json
from my_app.configs import app_settings
from django.conf import settings as project_settings

from my_app.configs.service_api_config import SERVICE_CONFIG
from my_app.my_core.helpers import utils


class MyPtHoProfileApis:
    base_uri = ""

    def __init__(self):
        domainName = ""
        appEnv = str(project_settings.APP_ENVIRONMENT)
        if appEnv == "staging":
            # print("MyPtHOProfileApis : moi truong staging")
            domainName = app_settings.MYPT_HO_PROFILE_STAGING_DOMAIN_NAME
        elif appEnv == "production":
            # print("MyPtHOProfileApis : moi truong production")
            domainName = app_settings.MYPT_HO_PROFILE_PRODUCTION_DOMAIN_NAME
        else:
            # print("MyPtHOProfileApis : moi truong local")
            domainName = app_settings.MYPT_HO_PROFILE_LOCAL_DOMAIN_NAME

        self.base_uri = domainName + "/mypt-ho-profile-api"

    def getProfileInfo(self, userId, email, fullName, extra={}):
        apiUrl = self.base_uri + "/get-profile-info"
        inputParamsStr = json.dumps({
            "userId": int(userId),
            "email": email,
            "fullName": fullName,
            "specificChildDeparts": extra.get("collectedSpecificChildDeparts", []),
            "specificAgencies": extra.get("collectedSpecificAgencies", []),
            "specificParentdDeparts": extra.get("collectedSpecificParentDeparts", [])
        })
        headersDict = {
            "Content-Type": "application/json"
        }
        # print("URL mypt-ho-profile-api get profile info : " + apiUrl + " ; " + inputParamsStr)
        # print(headersDict)

        try:
            responseObj = requests.post(apiUrl, headers=headersDict, data=inputParamsStr, timeout=5)
            print("Result call API mypt-ho-profile get profile info : ", responseObj)
            responseData = json.loads(responseObj.text)
            # return responseData

            if responseData.get("statusCode", None) is None:
                return None

            infoData = None
            resCode = int(responseData.get("statusCode"))
            if resCode == 1:
                infoData = responseData.get("data")

            return infoData
        except Exception as ex:
            print("call /get-profile-info err/loi: {}".format(ex))
            if isinstance(ex, requests.exceptions.ReadTimeout):
                # logger.info("api_auth :Connection Timeout")
                print("api_auth :Connection Timeout")
            if isinstance(ex, requests.exceptions.ConnectionError):
                # logger.info("api_auth :Connection Error")
                print("api_auth :Connection Timeout")

        return None

    def healthCheck(self):
        apiUrl = self.base_uri + "/health"

        # print("URL mypt-ho-profile-api health : " + apiUrl)

        try:
            responseObj = requests.get(apiUrl, timeout=5)
            # print("Result call API mypt-ho-profile-api health : " + responseObj.text)
            responseData = json.loads(responseObj.text)
            # return responseData

            if responseData.get("statusCode", None) is None:
                return None

            infoData = None
            resCode = int(responseData.get("statusCode"))
            if resCode == 1:
                infoData = responseData.get("data")

            return infoData
        except Exception as ex:
            if isinstance(ex, requests.exceptions.ReadTimeout):
                # logger.info("api_auth :Connection Timeout")
                print("api_auth :Connection Timeout")
            if isinstance(ex, requests.exceptions.ConnectionError):
                # logger.info("api_auth :Connection Error")
                print("api_auth :Connection Timeout")

        return None

    def checkEmailHasFeatureRolesByCodes(self, email, feaRolesCodes):
        base_env = project_settings.APP_ENVIRONMENT
        apiUrl = self.base_uri + "/check-email-has-fea-roles-by-codes"
        api_url = SERVICE_CONFIG["HO-PROFILE"][base_env] + \
                  SERVICE_CONFIG["HO-PROFILE"]["check_email_has_fea_roles_by_codes"]["func"]
        inputParamsStr = {
            "email": email,
            "feaRolesCodes": feaRolesCodes
        }
        print("URL mypt-ho-profile-api check-email-has-fea-roles-by-codes : " + api_url + " ; " + str(inputParamsStr))

        try:
            # responseObj = requests.post(apiUrl, headers=headersDict, data=inputParamsStr)
            responseObj = utils.call_api(
                host=SERVICE_CONFIG["HO-PROFILE"][base_env],
                func=SERVICE_CONFIG["HO-PROFILE"]["check_email_has_fea_roles_by_codes"]["func"],
                method=SERVICE_CONFIG["HO-PROFILE"]["check_email_has_fea_roles_by_codes"]["method"],
                data=inputParamsStr
            )
            responseStr = responseObj
            print("Result call API mypt-ho-profile check-email-has-fea-roles-by-codes : " + responseStr)
            responseData = json.loads(responseStr)
            if responseData.get("statusCode", None) is None:
                return None
            infoData = None
            resCode = int(responseData.get("statusCode"))
            if resCode == 1:
                infoData = responseData.get("data")
                return infoData
            return infoData
        except Exception as ex:
            print(ex)
            return None
